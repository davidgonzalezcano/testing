package cat.itb.testing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class WelcomeBackActivity extends AppCompatActivity {
    private Button buttonback;
    private TextView welcomeback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_back_activity);

        buttonback = findViewById(R.id.buttonback);
        welcomeback = findViewById(R.id.welcomeback);

        String user = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user = bundle.getString("user");
        }
        welcomeback.setText(welcomeback.getText().toString() + " " + user);

        buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeBackActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
