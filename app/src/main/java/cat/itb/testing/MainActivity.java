package cat.itb.testing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private TextView mainActivityTitle;
    private Button buttonNext;

    private EditText username;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivityTitle = findViewById(R.id.mainActivityTitle);
        buttonNext = findViewById(R.id.buttonNext);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonNext.setText("Logged");
                Intent intent = new Intent(MainActivity.this, WelcomeBackActivity.class);
                String user = username.getText().toString();
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });
    }
}