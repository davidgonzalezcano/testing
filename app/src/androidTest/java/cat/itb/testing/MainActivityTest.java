package cat.itb.testing;

import android.content.Context;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> publicActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);
/*
    @Test
    public void elements_On_MainActivity_Are_Displayed() {
        onView(withId(R.id.mainActivityTitle)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonNext)).check(matches(isDisplayed()));
    }


    @Test
    public void elements_On_Mainactivity_Text_Is_Correct() {
        onView(withId(R.id.mainActivityTitle)).check(matches(withText("Main Activity Title")));
        onView(withId(R.id.buttonNext)).check(matches(withText("Next")));
    }

    @Test
    public void elements_On_Mainactivity_Check_Button_Is_Clicable_And_Change_Text() {
        onView(withId(R.id.buttonNext)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.buttonNext)).check(matches(withText("Logged")));
    }

    @Test
    public void login_form_behaviour(){
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String usernameText = context.getResources().getString(R.string.USER_TO_BE_TYPED);
        onView(withId(R.id.username)).perform(replaceText(usernameText));
        Espresso.closeSoftKeyboard();

        String passwordText = context.getResources().getString(R.string.PASS_TO_BE_TYPED);
        onView(withId(R.id.password)).perform(replaceText(passwordText));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.buttonNext)).check(matches(withText("Logged")));
    }

    @Test
    public void when_click_Button_go_to_welcomeBackActivity() {
        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.welcomebackactivity)).check(matches(isDisplayed()));
    }


    @Test
    public void when_click_Button_go_to_MainActivity() {
        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.welcomebackactivity)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonback)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
    }
 */

    @Test
    public void Jira() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String usernameText = context.getResources().getString(R.string.USER_TO_BE_TYPED);
        onView(withId(R.id.username)).perform(replaceText(usernameText));
        Espresso.closeSoftKeyboard();

        String passwordText = context.getResources().getString(R.string.PASS_TO_BE_TYPED);
        onView(withId(R.id.password)).perform(replaceText(passwordText));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.welcomebackactivity)).check(matches(isDisplayed()));

        onView(withId(R.id.welcomeback)).check(matches(withText("Welcome back " + usernameText)));

        onView(withId(R.id.buttonback)).perform(click());

        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));

        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }
}
